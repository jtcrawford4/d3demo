$( document ).ready(function() {
    Chart.defaults.global.elements.line.fill = false;

    var ctx = document.getElementById("myChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
            datasets: [{
                data: [86,114,106,106,107,111,133,221,783,2478],
                    label: "Africa",
                    backgroundColor: "#2980b9",
                    borderColor: "#2980b9",
                  }, {
                data: [282,350,411,502,635,809,947,1402,3700,5267],
                    label: "Asia",
                    backgroundColor: "#8e44ad",
                    borderColor: "#8e44ad",
                  }, {
                data: [168,170,178,190,203,276,408,547,675,734],
                    label: "Europe",
                    backgroundColor: "#27ae60",
                    borderColor: "#27ae60",
                  }, {
                data: [40,20,10,16,24,38,74,167,508,784],
                    label: "Latin America",
                    backgroundColor: "#f39c12",
                    borderColor: "#f39c12",
                  }, {
                data: [6,3,2,2,7,26,82,172,312,433],
                    label: "North America",
                    backgroundColor: "#c0392b",
                    borderColor: "#c0392b",
                  },
            ]
        },
         options: {
            responsive: false,
            title: {
              display: true,
              text: 'World population per region (in millions)',
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    $("#toggleEast").click(function() {
        myChart.data.datasets.forEach(function(ds) {
        if (ds.label.match("Africa|Europe|Asia")){
            ds.hidden = !ds.hidden;
        }
      });
      myChart.update();
    });

    $("#toggleWest").click(function() {
        myChart.data.datasets.forEach(function(ds) {
        if (ds.label.match("North America|Latin America")){
            ds.hidden = !ds.hidden;
        }
      });
      myChart.update();
    });
});