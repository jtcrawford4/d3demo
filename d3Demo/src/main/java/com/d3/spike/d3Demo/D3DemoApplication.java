package com.d3.spike.d3Demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class D3DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(D3DemoApplication.class, args);
	}
}
